# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Fixed

- The artifact mask matches a package in the pipeline stage 'build'.

## [1.0.0] - 2018-07-02

### Added

- The configuration example with roles using this module.
- The fakt class creates $facts_d_dir if it is absent.

## [0.2.0] - 2018-06-26

### Added

- The fact value may be missing if the fact is removing.

### Fixed

- Multiple fact variables can be used.

## [0.1.2] - 2018-06-26

### Fixed

- Hiera lookup_options.

## [0.1.1] - 2018-06-26

### Added

- Hiera v5 default values.
- Tags in metadata.json.

### Fixed

- the README file has a valid title.

## 0.1.0 - 2018-06-25

### Added

- Initial classes and resources.

[Unreleased]: https://gitlab.com/bibigon812/puppet-fakts/compare/v1.0.0...master
[1.0.0]: https://gitlab.com/bibigon812/puppet-fakts/compare/v0.2.0...v1.0.0
[0.2.0]: https://gitlab.com/bibigon812/puppet-fakts/compare/v0.1.2...v0.2.0
[0.1.2]: https://gitlab.com/bibigon812/puppet-fakts/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/bibigon812/puppet-fakts/compare/v0.1.0...v0.1.1
