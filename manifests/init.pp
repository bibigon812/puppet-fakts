# This class manages custom facts. Facts in the JSON format are stored
# into the directory '/etc/facter/facts.d' by default.
#
# @summary Manages custom facts.
#
# @example
#   include fakts
#
#   fakts::static:
#     groups:
#       ensure: present
#       value:
#         - foo
#         - bar
#     testfact1:
#       ensure: absent
#       value: baz
#
# @param facts_d_dir
#   A default facts directory.
# @param static
#   Static facts.
class fakts (

  Stdlib::Absolutepath $facts_d_dir,
  Hash[String[1], Struct[{
    ensure => Optional[Enum['absent', 'present']],
    value  => Any,
  }]]                  $static,

) {
  exec { "mkdir -p ${facts_d_dir}":
    path   => ['/bin', '/usr/bin'],
    unless => "test -d ${facts_d_dir}",
    before => File[$facts_d_dir],
  }

  file { $facts_d_dir:
    ensure => 'directory',
  }

  $static.each |$fact_name, $fact_opts| {
    fakts::fakt { $fact_name:
      facts_d_dir => $facts_d_dir,
      *           => $fact_opts,
    }
  }
}
