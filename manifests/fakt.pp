# This resource manages custom fact.
#
# @summary Manages a custom fact.
#
# @example
#   fakts::fakt { 'namevar':
#     ensure      => 'present',
#     facts_d_dir => '/etc/facter/facts.d',
#     value       => 'value',
#   }
#
# @param ensure
#   Valid values are 'absent', 'present'.
# @param facts_d_dir
#   An absolute path where fact will be stored.
# @param value
#   A fact value.
define fakts::fakt (
  Stdlib::Absolutepath      $facts_d_dir,
  Enum['absent', 'present'] $ensure = 'present',
  Any                       $value  = undef,
) {
  if $ensure == 'present' and $value == undef {
    fail ("The fakt '${name}' has no value.")
  }

  $fakt = { $name => $value }
  $filename = regsubst($name, '[/ ]', '_', 'G')

  file { "${facts_d_dir}/${filename}.json":
    ensure  => $ensure,
    content => template('fakts/fakt.json.erb'),
    require => File[$facts_d_dir],
  }
}
