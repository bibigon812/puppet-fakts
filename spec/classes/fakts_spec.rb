require 'spec_helper'

describe 'fakts' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
      it { is_expected.to contain_exec('mkdir -p /etc/facter/facts.d') }
      it { is_expected.to contain_file('/etc/facter/facts.d') }
    end
  end
end
