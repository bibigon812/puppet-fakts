require 'spec_helper'

describe 'fakts::fakt' do
  let(:pre_condition) do
    <<-MANIFEST
      exec { 'mkdir -p /etc/facter/facts.d':
        path   => ['/bin', '/usr/bin'],
        unless => 'test -d /etc/facter/facts.d',
      }

      file { '/etc/facter/facts.d':
        ensure => 'directory',
      }
    MANIFEST
  end

  let(:title) { 'groups' }
  let(:params) do
    {
      facts_d_dir: '/etc/facter/facts.d',
      value:       %w[foo bar],
    }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
      it { is_expected.to contain_file('/etc/facter/facts.d/groups.json').with_content(%r{\{"groups":\["foo","bar"\]\}}) }
    end
  end
end
