
# fakts

This module manages facts via files.

#### Table of Contents

1. [Description](#description)
2. [Usage - Configuration options and additional functionality](#usage)
3. [Reference - An under-the-hood peek at what the module is doing and how](#reference)

## Description

This class manages custom facts. Facts in the JSON format are
stored into the directory '/etc/facter/facts.d' by default.

## Usage

```puppet
include fakts
```

```yaml
fakts::static:
  groups:
    value:
      - foo
      - bar
```

### Roles

if you are going to manage roles, you can use this module to push roles to
nodes.

Prepare the hiera configuration file
```yaml
# hiera.yaml
---
version: 5
defaults:
hierarchy:
  - name: "Per-node data (yaml version)"
    path: "nodes/%{::trusted.certname}.yaml"
  - name: "Per-role data (yaml version)"
    mapped_paths: [roles, var, "roles/%{var}.yaml"]
```

```puppet
# manifests/site.pp
node default {
  lookup('classes', Array[String[1]], 'unique', []).include
}
```

Add role names into hiera.

```yaml
# %{::trusted.certname}.yaml
---
classes:
  - fakts

fakts::static:
  roles:
    - foo
    - bar
```

Hiera will search in the 'roles/foo.yaml' and 'roles/bar.yaml'

## Reference

### Class Fakts

#### Summary

Manages custom facts.

#### Overview

This class manages custom facts. Facts in the JSON format are stored into the directory '/etc/facter/facts.d' by default.

#### Examples

```puppet
class { 'fakts':
  'static' => {
    'groups' => {
      'value' => [
        'foo',
        'bar',
      ],
    },
  },
}
```

#### Parameters

- facts_d_dir (Stdlib::Absolutepath) (defaults to: '/etc/facter/facts.d') —
  A default facts directory.
- static (Hash[String[1], Struct[{ ensure => Optional[Enum['absent',
  'present']], value => Any, }]]) — Static facts.

### Defined Type Fakts::Fakt

#### Summary

Manages a custom fact.

#### Overview

This resource manages custom fact.

#### Examples

```puppet
fakts::fakt { 'namevar':
  ensure      => 'present',
  facts_d_dir => '/etc/facter/facts.d',
  value       => 'value',
}
```

#### Parameters

- ensure (Enum['absent', 'present']) (defaults to: 'present') — Valid values
  are 'absent', 'present'.
- facts_d_dir (Stdlib::Absolutepath) — An absolute path where a fact will be
  stored.
- value (Any) (defaults to: undef) — A fact value.
